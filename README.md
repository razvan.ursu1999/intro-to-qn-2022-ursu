# Final Project - Introduction to Quantum Networks - WiSe 2021-2022
## Author: Răzvan-Mihai Ursu
### Date 08.03.2022

This is my final project for the course Introduction to Quantum Networks - WiSe 2021-2022.  

## Repository contents

As a broad overview, this repository contains the following:
- The plots of the obtained results (plots of needed time vs number of nodes or number of entanglment units) 
    - `time_vs_no_nodes_all.pdf`
    - `time_vs_no_nodes.pdf`
    - `time_vs_no_swaps_all.pdf`
    - `time_vs_no_swaps.pdf`
- The Jupyter Notebook that has been used to generate these plots `eda.ipynb`. This notebook generates the plots with the data in `data.csv`.
- This `README.md`
- The folder `extended_homework` with the homework and the code per se.

The protocols are to be found in `extended_homework/entanglement_swapping`.

The `main.py` is the file I have used for generating the results. As the time is dependent also on the equipment you are using, the absolute times may differ, however the pattern that can be seen in the plots should be the same (as it is based on the theoretical model)

## Running some tests
I have also written tests in order to check the validity of my functions in some simple cases. These can be found under `extended_homework/tests`, and their purpose is explained as documentation in the source code 

If you would also like to run these tests, here are some instructions:
1. Clone this repository locally
2. Create a python virtual environment, activate it and install the needed packages \
`python3 -m venv venv` \
`./venv/bin/activate` \
`python3 -m pip install -r ./extended_homework/requirements.txt` \
`python3 -m pip install pytest`
3. Change the working directory to `./extended_homework/`
4. Run
`python3 -m pytest -s` The -s option enables the real time visualisation of the outputs of the print statements.

## Documentation
The code is documented and the steps of the protocols are explained in the source code.

## Presentation
This code will be accompanied by a PPT presentation.
