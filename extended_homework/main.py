""" Main program for generating all the measurements. """

from typing import List, Dict, Tuple
import time
import datetime
from itertools import product

from qunetsim.components.network import Network
from qunetsim.objects import Logger


Logger.DISABLED = True

from entanglement_swapping.general_functions import setup_network_connections
from entanglement_swapping.protocol_DSTRB import create_network_epr_pairs, send_recv_entanglement_swap, \
                                            get_scheduling_nodes, measure_first_last


def main():
    """ The main program generates the results for all the tests.
        
        We run 20 repetitions for all possible values of N (4, ..., 17) and
        for all values of K (1, ..., 25).

        We measure the times and save all the data in the "data.csv" file. 
    """

    # Number of repetitions
    REPS = 20

    # Number of nodes
    N_list = list(range(2 ** 2, 2 ** 4 + 1))

    # Number of entanglement units
    K_list = list(range(1, 25))

    # Create all configs
    configs = list(product(*[N_list, K_list, list(range(REPS))]))

    # Get the last configuration (REP_NO, N, K) which has run succesfully.
    # We continue with the following configuration
    try:
        with open("last_entry.txt", "r") as f:
            last_entry = int(f.read())
        SLICE = last_entry + 1
    except:
        SLICE = 0

    configs = configs[SLICE:]

    # Iterate through all remaining configurations
    for i, (N, K, rep_no) in enumerate(configs, start=SLICE):
        try:
            print(f">>>>>>>> Test: {i} >>> N: {N}, K: {K}, rep: {rep_no}")

            # Setup the network and the schedule for DSTRB
            # In the network setup we also create the EPR pairs between
            # the different hosts in our linear network.
            network, schedule = setup_DSTRB(K, N)

            # Start the timer
            tic = time.perf_counter()

            # Run the DSTRB protocol over the whole network
            run_DSTRB(network, schedule, K, N)

            # Stop the timer
            toc = time.perf_counter()

            # Check if the protocol ran successfully (if there are no errors)
            # for the measurements
            error_count = evaluation_DSTRB(network, K, N)
            
            if error_count == 0:
                write_to_file(K, N, rep_no, toc-tic, error_count)
                with open("last_entry.txt", "w") as f:
                    f.write(str(i))
                # print(f"It took {toc - tic:0.4f} seconds")
        except:
            network.stop(True)
            rep_no -= 1


# Line is: date, K, N, REP_NO, TIME, ERROR_COUNT
def write_to_file(K: int, N: int, rep_no: int, time: float, error_count: int):
    """Write the measurement results to the file

    Args:
        K (int): number of entanglement units
        N (int): number of hosts
        rep_no (int): index of repetition
        time (float): time needed in seconds
        error_count (int): number of errors
    """
    now = datetime.datetime.now()
    date_time = now.strftime("%d/%m/%Y-%H:%M:%S")
    with open("data.csv", "a+") as f:
        line = f"{date_time}; {K}; {N}; {rep_no}; {time:0.4f}; {error_count}\n"
        f.write(line)
        

def setup_DSTRB(K: int, N: int) -> Tuple[Network, List[Dict[int, List[int]]]]:
    """ Setup the network.

    Args:
        K (int): number of entanglement units
        N (int): number of hosts

    Returns:
        Tuple[Network, List[Dict[int, List[int]]]]: network and schedule
    """
    # Setup the network and the connections
    network = setup_network_connections(N)

    # Create the network EPR pairs
    create_network_epr_pairs(network, K, N)

    # Generate the schedule, as a function of the number of nodes
    schedule = get_scheduling_nodes(N)

    return network, schedule

def run_DSTRB(network: Network, schedule: List[Dict[int, List[int]]], K: int, N: int):
    """ Run the entanglement swapping protocol 

    Args:
        network (Network): the network
        schedule (List[Dict[int, List[int]]]): the schedule
        K (int): number of entanglement units
        N (int): number of hosts
    """

    # Iterate through all of the rounds in the schedule
    for round, round_schedule in enumerate(schedule):
        round_thread_list = [] 

        # Iterate through all of the hosts in the network
        for i in range(N):
            host = network.get_host(str(i))

            # Run the send_recv_entanglement_swap for this host 
            round_thread = host.run_protocol(send_recv_entanglement_swap, (round_schedule, K, N))

            # Create threads with the protocol running for each of the hosts
            round_thread_list.append(round_thread)

        # Join all threads
        for round_thread in round_thread_list:
            round_thread.join()

def evaluation_DSTRB(network: Network, K: int, N: int) -> int:
    """Evaluate the results of the DSTRB protocol

    Args:
        network (Network): the network
        K (int): number of entanglement units
        N (int): number of hosts

    Returns:
        int: the error count
    """
    error_count = measure_first_last(network, K, N)

    network.stop(True)
    return error_count


if __name__ == '__main__':
    main()