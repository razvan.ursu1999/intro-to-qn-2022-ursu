#requires -Version 2

$maximumRuntimeSeconds = 24000

while($true)
{
    $process = Start-Process -FilePath powershell.exe -ArgumentList '../venv/Scripts/python.exe ./main.py' -PassThru

    try
    {
        Start-Sleep 5
        $process | Wait-Process -Timeout $maximumRuntimeSeconds -ErrorAction Stop
        Write-Warning -Message 'Process successfully completed within timeout.'
    }
    catch
    {
        Write-Warning -Message 'Process exceeded timeout, will be killed now.'
        Start-Sleep 5
        $process | Stop-Process -Force
    }
}