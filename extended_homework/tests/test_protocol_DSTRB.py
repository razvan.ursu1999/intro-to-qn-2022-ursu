from entanglement_swapping.protocol_DSTRB import get_scheduling_nodes, create_network_epr_pairs, \
                                                 send_recv_entanglement_swap, measure_first_last
from entanglement_swapping.general_functions import setup_network_connections

N = 2 ** 3
K = 7
ANSWER = [{1: [0, 2], 3: [2, 4],  5: [4, 6]}, 
          {2: [0, 4], 6: [4, 7]}, 
          {4: [0, 7]}]


def test_scheduling():
    """ Test the scheduling for 8 users
    """
    assert get_scheduling_nodes(8) == ANSWER

def test_accuracy_DSTRB():
    """ Test the accuracy of DSTRB. Run the protocol for 8 hosts and 7 
        entanglement units and expect 0 errors.
    """
    network = setup_network_connections(N)

    create_network_epr_pairs(network, K, N)

    schedule = get_scheduling_nodes(N)

    for round, round_schedule in enumerate(schedule):
        print(f">>>>>>>> Round {round}")
        round_thread_list = [] 
        for i in range(N):
            host = network.get_host(str(i))
            round_thread = host.run_protocol(send_recv_entanglement_swap, (round_schedule, K, N))
            round_thread_list.append(round_thread)

        for round_thread in round_thread_list:
            round_thread.join()

    assert measure_first_last(network, K, N) == 0

    network.stop(False)

    