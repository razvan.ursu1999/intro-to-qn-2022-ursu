""" Test the classical protocols """
import pytest

from entanglement_swapping.general_functions import setup_network_connections
from entanglement_swapping.protocol_CL import send_protocol_CL, recv_protocol_CL

N = 2 ** 2
# INPUTS = ["01"]
INPUTS = ["1", "011", "", "010010010011001101011011011011010010111010101",
        "1000101000100101101000100011001100001110010001111010100110110101010110010010110000100001111110110110101101101011101100101001000110000000000101110110011000010110111101010110100011110100111011001011100000100001000111111110111101110001101010001100110100110100100001101110100001100001011001101110001101111101110111101101100110001011110000111100110000011000101110111101100010100111111110101001100101001010110100111100111101001011010101010111111111101000101100001010110100011011001111100100010101010100111010111101001110000011110000101101111100101110101110010101001100010101101111111100010100101001010000000001101011111101001110011001000100111000011101110001100001000011101110001110110111000100010110000101001101001101011110101000100101001100001000111001001001001111000001011111001101101111100000000001000010101011000100010110001001011110001000111011110101101010011101001000101111100101101110010010011001100000001110000011111101111001111111010000010001110010001110000110000011110100101100001000010010011110111101110001101011100101"]

LENGTHS = [1, 10, 20]

@pytest.mark.parametrize("input", INPUTS)
def test_transmissions_adjacent_hosts(input: str):
    """ Test the transmission between adjacent hosts

    Args:
        input (str): the input bitstring
    """
    # set INPUT and OUTPUT
    output = [""]

    # Setup the network and the connections
    network = setup_network_connections(N)

    # Set the hosts and run the protocols
    host_A = network.get_host('0')
    host_B = network.get_host('1')
    t1 = host_A.run_protocol(send_protocol_CL, (host_B.host_id, input,))
    t2 = host_B.run_protocol(recv_protocol_CL, (host_A.host_id, output,), blocking=True)

    # Stop the network    
    network.stop(True)

    assert output[0] == input


def test_transmissions_far_hosts():
    ''' Test the classical transmission protocol between two hosts far apart from each other '''
    # set INPUT and OUTPUT
    input = INPUTS[0]
    output = [""]

    # Setup the network and the connections
    network = setup_network_connections(N)

    # Set the hosts and run the protocols
    host_A = network.get_host('0')
    host_B = network.get_host('2')
    t1 = host_A.run_protocol(send_protocol_CL, (host_B.host_id, input,))
    t2 = host_B.run_protocol(recv_protocol_CL, (host_A.host_id, output,), blocking=True)

    # Stop the network    
    network.stop(True)

    assert output[0] == input

@pytest.mark.parametrize("expected_length", LENGTHS)
def test_transmissions_expected_length(expected_length: int):
    """ Test the classical transmission protocol between two hosts which know how many bits they should
        receive 

    Args:
        expected_length (int): the expected transmission length
    """

    # set INPUT and OUTPUT
    input = INPUTS[3]
    output = [""]

    # Setup the network and the connections
    network = setup_network_connections(N)

    # Set the hosts and run the protocols
    host_A = network.get_host('0')
    host_B = network.get_host('1')
    t1 = host_A.run_protocol(send_protocol_CL, (host_B.host_id, input,))
    t2 = host_B.run_protocol(recv_protocol_CL, (host_A.host_id, output, expected_length), blocking=True)

    # Stop the network    
    network.stop(True)

    assert output[0] == input[:expected_length]