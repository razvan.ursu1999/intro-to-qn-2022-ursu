""" Some general functions, independent of the used protocol. """

from qunetsim.components.host import Host
from qunetsim.components.network import Network

def setup_network_connections(N: int) -> Network:
    """ Setup the linear network with N users (both 
        quantum and classical connections)

    Args:
        N (int): number of hosts

    Returns:
        Network: the network
    """
    network = Network.get_instance()

    # The node names are NUMBERS
    nodes = [str(i) for i in range(N)]
    network.start(nodes)

    assert N > 1

    # Although we should only establish quantum connections, there is
    # a bug (https://github.com/tqsd/QuNetSim/issues/44) and a qubit
    # transmission also needs a classical connection. Therefore we will
    # use add_connection, instead of add_q_connection (suggested by the
    # Final Project 3)

    # setup Host 0
    host_0 = Host(nodes[0])
    host_0.add_connection(nodes[1])
    host_0.start()
    network.add_host(host_0)

    # setup Host N-1
    host_last = Host(nodes[-1])
    host_last.add_connection(nodes[-2])
    host_last.start()
    network.add_host(host_last)

    # setup the other Hosts
    for i in range(1, N-1):
        host = Host(nodes[i])
        host.add_connections([nodes[i-1], nodes[i+1]])
        host.start()
        network.add_host(host)

    return network
