""" Implement the DSTRB protocol """

from operator import itemgetter
from typing import List, Dict

from qunetsim.components.host import Host
from qunetsim.components.network import Network
from qunetsim.objects import Qubit

from .protocol_CL import send_protocol_CL, recv_protocol_CL

def send_epr_pair_DSTRB(host: Host, recv_id: str, K: int) -> None:
    """ Sending function - distribute the EPR pairs

    Args:
        host (Host): the sending host
        recv_id (str): the receiving host id
        K (int): the number of entanglement units
    """
    for _ in range(K):
        epr_id, ack_arrived = host.send_epr(recv_id, await_ack=True)

        if ack_arrived:
            q = host.get_epr(recv_id, q_id=epr_id)
            host.add_epr(recv_id, q)
            print(f"Send EPR_id from {host.host_id} to {recv_id} = {epr_id} --- ACK_arrived = {ack_arrived}")


def recv_epr_pair_DSTRB(host: Host, send_id: str, K: int) -> None:
    """ Receiving function - distribute the EPR pairs

    Args:
        host (Host): the receiving host
        send_id (str): the sending host id
        K (int): number of entanglement units
    """

    for _ in range(K):
        q = host.get_epr(send_id, wait=60)
        # time.sleep(5)
        host.add_epr(send_id, q)
        print(f"Recv EPR_id from {send_id} to {host.host_id} = {q.id}")


def create_network_epr_pairs(network: Network, K: int, N: int) -> None:
    """ Create in the network all EPR pairs needed for the DSTRB protocol

    Args:
        network (Network): the network
        K (int): number of entanglement units
        N (int): number of hosts
    """
    send_threads = []
    recv_threads = []
    for i in range(N - 1):
        host_A = network.get_host(str(i))
        host_B = network.get_host(str(i+1))
        t1 = host_A.run_protocol(send_epr_pair_DSTRB, (host_B.host_id, K,), blocking=False)
        t2 = host_B.run_protocol(recv_epr_pair_DSTRB, (host_A.host_id, K,), blocking=False)

        send_threads.append(t1)
        recv_threads.append(t2)
    
    for send_thread, recv_thread in zip(send_threads, recv_threads):
        send_thread.join()
        recv_thread.join()

def get_scheduling_nodes(N: int) -> List[Dict[int, List[int]]]:
    """ Create the scheduling of the nodes according to the logic of the 
        DSTRB protocol.

    Args:
        N (int): number of hosts

    Returns:
        List[Dict[int, List[int]]]: list of dictionaries containing the schedule
            The entries of the list (Dict) represent the different scheduling rounds and
            the Dicts contain the pair values (sending host : list of receiving hosts).
        
    For instance, in the case of 8 hosts, the answer should be:
    schedule = [{1: [0, 2], 3: [2, 4],  5: [4, 6]}, 
          {2: [0, 4], 6: [4, 7]}, 
          {4: [0, 7]}]
    """
    schedule = []
    all_nodes = list(range(N))
    while len(all_nodes) > 2:
        sending_nodes = {}
        indices_to_delete = list(range(1, len(all_nodes)-1, 2))

        for idx in indices_to_delete:
            sending_nodes[all_nodes[idx]] = [all_nodes[idx-1], all_nodes[idx+1]]

        if len(indices_to_delete) > 1:
            for item in (itemgetter(*indices_to_delete)(all_nodes)):
                all_nodes.remove(item)
        elif len(indices_to_delete) == 1:
            item = all_nodes[indices_to_delete[0]]
            all_nodes.remove(item)
            
        schedule.append(sending_nodes)

    # print(f"The schedule is {schedule}.")
    return schedule

def is_sending_current_round(index: int, schedule: Dict[int, List[int]]) -> bool:
    """ Check if the host is scheduled to send this round

    Args:
        index (int): round index
        schedule (Dict[int, List[int]]): the schedule

    Returns:
        bool: True if the host should be sending
    """
    if index in schedule:
        return True
    return False

def is_receiving_current_round(index: int, schedule: Dict[int, List[int]]) -> bool:
    """ Check if the host is scheduled to receive this round

    Args:
        index (int): round index
        schedule (Dict[int, List[int]]): tbe schedule

    Returns:
        bool: is receiving
    """
    for sender in schedule:
        if index in schedule[sender]:
            return True
    return False

def get_sender_ids(index: int, schedule: Dict[int, List[int]]) -> List[int]:
    """ Get the ids of the hosts sending this round.

    Args:
        index (int): round index
        schedule (Dict[int, List[int]]): the schedule

    Returns:
        List[int]: the sender ids.
    """
    senders = []
    for sender in schedule:
        if index in schedule[sender]:
            senders.append(sender)
    return senders

def is_receiving_from_right(host: Host, sender: int) -> bool:
    """ Check if the receiving direction is from right

    Args:
        host (Host): the receiving host
        sender (int): the sending host id

    Returns:
        bool: is receiving from right
    """
    if int(host.host_id) < sender:
        return True
    return False

def custom_get_epr(host: Host, sender: int, epr_index: int) -> Qubit:
    """ Custom get epr.

        Check if the host is receiving from right or from left
        and choose the corresponding EPR Qubit that needs to be measured

    Args:
        host (Host): the receiving host
        sender (int): the sending host id
        epr_index (int): the index of the EPR pair (a number between 0 and K-1)

    Returns:
        Qubit: the corresponding qubit that needs to be measured.
    """

    if is_receiving_from_right(host, sender):
        neighbour = str(int(host.host_id) + 1)
        epr_pairs = host.get_epr_pairs(neighbour)
        return epr_pairs[epr_index]
    else:
        neighbour = str(int(host.host_id) - 1)
        epr_pairs = host.get_epr_pairs(neighbour)
        return epr_pairs[epr_index]

def update_epr(host: Host, sender: int, q: Qubit, bit: str):
    """ Change the qubit state according to the clasically received bit
        as specified by the Entanglement Swapping protocol.

    Args:
        host (Host): the receiving host
        sender (int): the sending host id
        q (Qubit): the Qubit whose state needs to be changed.
        bit (str): the clasically received bit
    """
    if bit == "1":
        if is_receiving_from_right(host, sender):
            q.Z()
        else:
            q.X()

def send_recv_entanglement_swap(host: Host, round_schedule: Dict[int, List[int]], K: int, N: int):
    """ Run the entanglement swapping protocol - both send and receive

    Args:
        host (Host): the current host
        round_schedule (Dict[int, List[int]]): the schedule of this round
        K (int): number of entanglement units
        N (int): number of hosts
    """
    index = int(host.host_id)

    if is_sending_current_round(index, round_schedule):
        # Make measurement and send the bits to the nearby sources
        left_host_id = round_schedule[index][0]
        right_host_id =  round_schedule[index][1]

        q_left_list = host.get_epr_pairs(str(index - 1))
        q_right_list = host.get_epr_pairs(str(index + 1))

        for i in range(K):
            q_left = q_left_list[i]
            q_right = q_right_list[i]

            # Make measurement
            meas = [None, None]
            q_left.cnot(q_right)
            q_left.H()
            meas = [str(q_left.measure()), str(q_right.measure())]


            # Run the classical sending protocol
            t1 = host.run_protocol(send_protocol_CL, (str(left_host_id), meas[0]))
            t2 = host.run_protocol(send_protocol_CL, (str(right_host_id), meas[1]))
            t1.join()
            t2.join()
    else:
        # Receive the bits from the nearby hosts, perform the transformations
        if not is_receiving_current_round(index, round_schedule):
            # If a user is neither receiving nor sending that means that it is idle
            # and we can skip it
            return
        
        sender_ids = get_sender_ids(index, round_schedule)
        for sender_id in sender_ids:
            output = [""]

            # If the host is receiving we run the corresponding classical receiving protocol
            t1 = host.run_protocol(recv_protocol_CL, (str(sender_id), output, K))
            t1.join()
            # print(f"User {index} has received bits: {output[0]} from sender {sender_id}")
            
            for pos in range(K):
                # Using all the classically received bits, we update the state of the qubits
                q = custom_get_epr(host, sender_id, pos)
                update_epr(host, sender_id, q, output[0][pos])

def measure_first_last(network: Network, K: int, N: int) -> int:
    """ Calculate the errors between the qubits of the first
        and of the last host at the end of the DSTRB protocol

    Args:
        network (Network): the network
        K (int): number of entanglement units
        N (int): number of hosts

    Returns:
        int: error count
    """
    host_first = network.get_host(str(0))
    host_last = network.get_host(str(N-1))

    epr_first = host_first.get_epr_pairs(str(1))
    epr_last = host_last.get_epr_pairs(str(N-2))

    print(epr_first)
    print(epr_last)

    error_count = 0
    for i in range(K):
        meas = [str(epr_first[i].measure()), str(epr_last[i].measure())]
        if meas[0] != meas[1]:
            error_count += 1
    # print(f"Error rate is {error_count / K : .2f}.")
    return error_count
        



