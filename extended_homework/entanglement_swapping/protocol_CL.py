""" Implement the CL protocol"""

from typing import Optional, Tuple

from qunetsim.components.host import Host
from qunetsim.components.network import Network
from qunetsim.objects import Qubit
from qunetsim.objects import Logger
Logger.DISABLED = True

MAX_WAIT = 10

def encode_CL(host: Host, input: str) -> Tuple[int, Qubit]:
    """ Encode the input bits into a Qubits

    Args:
        host (Host): the host to whom the created qubit should belong
        input (str): the input bits

    Yields:
        Iterator[Tuple[int, Qubit]]: generator to encode the input bit into Qubits
    """
    for i, bit in enumerate(input):
        q = Qubit(host)
        if bit == '1':
            q.X()
        yield i, q

def send_protocol_CL(host: Host, receiver: str, input: str) -> None:
    """ Send using the CL protocol. (Encode bits in Qubits and 
        send the Qubits)

    Args:
        host (Host): the sending host
        receiver (str): the receiving host id
        input (str): the input to be sent
    """
    for i, qubit in encode_CL(host, input):
        # print(f'Sending qubit {i+1}.')
        # Send qubit and wait for an acknowledgement
        host.send_qubit(receiver, qubit, await_ack=True)
        # print('Qubit %d was received by %s.' % (i+1, receiver))


def recv_protocol_CL(host: Host, sender: str, output: list, expected_length: Optional[int] = None) -> None:
    """ Receive using the CL protocol. (Measure the Qubits)

    Args:
        host (Host): the receiving host
        sender (str): the sending host id
        output (list): the output
        expected_length (Optional[int], optional): the expected length of the transmission.
            If this value is known, after we receive the expected number of bits, we 
            can stop the receiving function, without needing to wait for a timeout.
            Defaults to None
    """
    # Here we write the protocol code for another host.
    # We are able to only receive 1 transmission

    # If we don't know the transmission length
    if expected_length is None:
        while True:
            # Wait for a qubit from Alice for 5 seconds.
            q = host.get_data_qubit(sender, wait=MAX_WAIT)
            # Measure the qubit and print the result.
            if not q:
                break
            measurement = str(q.measure())
            # print(f'{host.host_id} received a qubit in the {measurement} state.')
            output[0] += str(measurement)
    
    else:
        for _ in range(expected_length):
            # Wait for a qubit from Alice for MAX_WAIT seconds.
            q = host.get_data_qubit(sender, wait=MAX_WAIT)
            # Measure the qubit and print the result.
            if not q:
                break
            measurement = str(q.measure())
            # print(f'{host.host_id} received a qubit in the {measurement} state.')
            output[0] += str(measurement)
